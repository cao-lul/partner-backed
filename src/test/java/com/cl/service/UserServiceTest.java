package com.cl.service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.cl.model.domain.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;


/**
 * 用户服务测试
 *
 * @author caolu
 * @date 2022/09/03
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testAddUser() {
        User user = new User();
        user.setUsername("jake");
        user.setUserAccount("123");
        user.setAvatarUrl("http://www.imeitou.com/qinglv/260882.html");
        user.setGender(0);
        user.setUserPassword("123");
        user.setPhone("123");
        user.setEmail("123");

        boolean result = userService.save(user);
        System.out.println(user.getId());
        Assertions.assertTrue(result);
    }

    @Test
    public void userRegistry() {
        //校验密码为空
        String userAccount = "cllld";
        String userPassword = "";
        String checkPassword = "123456";
        long result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        //账户小于四位
        userAccount = "cl";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        //密码小于八位
        userAccount = "cllld";
        userPassword = "123456";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        //非法字符
        userAccount = "cl lld";
        userPassword = "12345678";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        checkPassword = "123456789";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        //用户不能重复
        userAccount ="123";
        checkPassword = "12345678";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertEquals(-1, result);
        //注册一个用户
        userAccount = "cllld";
        result = userService.userRegistry(userAccount, userPassword, checkPassword);
        Assertions.assertTrue(result>0);
    }

    @Test
    public void searchUsers(){
        List<String> userList = Arrays.asList("java","python");

        List<User> user = userService.searchUsersByTag(userList);
        Assert.assertNotNull(user);
    }
}