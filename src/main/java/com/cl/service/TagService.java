package com.cl.service;

import com.cl.model.domain.Tag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【tag(标签)】的数据库操作Service
* @createDate 2022-09-27 11:25:00
*/
public interface TagService extends IService<Tag> {

}
