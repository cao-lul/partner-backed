package com.cl.service;

import com.cl.model.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Administrator
 * @description 针对表【user(用户)】的数据库操作Service
 * @createDate 2022-09-03 17:30:36
 */
public interface UserService extends IService<User> {
    /**
     * 用户注册表
     *
     * @param userAccount   用户帐户
     * @param userPassword  用户密码
     * @param checkPassword 检查密码
     * @return long 新用户id
     */
    long userRegistry(String userAccount, String userPassword, String checkPassword);

    /**
     * 做登录
     *
     * @param userAccount  用户帐户
     * @param userPassword 用户密码
     * @return {@link User}
     */
    User doLogin(String userAccount, String userPassword, HttpServletRequest request);

    /**
     * 得到安全用户(用戶脫敏)
     *
     * @param originUser 起源用户
     * @return {@link User}
     */
    User getSafetyUser(User originUser);


    /**
     * 用户注销
     *
     * @param request 请求
     */
    int userLogout(HttpServletRequest request);

    /**
     * 搜索用户标签
     *
     * @param tagNameList 标记名称列表
     * @return {@link List}<{@link User}>
     */
    List<User> searchUsersByTag(List<String> tagNameList);
}
