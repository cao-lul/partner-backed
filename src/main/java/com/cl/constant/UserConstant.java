package com.cl.constant;

/**
 * 用户不断
 *
 * @author caolu
 * @date 2022/09/04
 */
public interface UserConstant {

    /**
     * 用户登录状态
     */
    String USER_LOGIN_STATE = "userLoginState";
    /**
     * 管理角色
     */
    int ADMIN_ROLE = 1;
    /**
     * 默认角色
     */
    int DEFAULT_ROLE = 0;
}
