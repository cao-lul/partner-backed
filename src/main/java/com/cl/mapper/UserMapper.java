package com.cl.mapper;

import com.cl.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2022-09-03 17:30:36
* @Entity com.cl.model.User
*/
public interface UserMapper extends BaseMapper<User> {

}




