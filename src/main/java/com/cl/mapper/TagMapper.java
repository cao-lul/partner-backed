package com.cl.mapper;

import com.cl.model.domain.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【tag(标签)】的数据库操作Mapper
* @createDate 2022-09-27 11:25:00
* @Entity com.cl.model.domain.Tag
*/
public interface TagMapper extends BaseMapper<Tag> {

}




