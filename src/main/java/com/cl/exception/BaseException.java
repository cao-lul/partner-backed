package com.cl.exception;


import com.cl.common.ErrorCode;
import com.fasterxml.jackson.databind.ser.Serializers;

/**
 * 自定义异常类
 *
 * @author caolu
 * @date 2022/09/06
 */
public class BaseException extends RuntimeException{

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    private int code;

    private String description;

    public BaseException(ErrorCode errorCode){
        super(errorCode.getMessage());
        this.code=errorCode.getCode();
        this.description = errorCode.getDescription();
    }

    public BaseException(String message,int code,String description){
        super(message);
        this.code=code;
        this.description = description;
    }

    public BaseException(ErrorCode errorCode , String description){
        super(errorCode.getMessage());
        this.code= errorCode.getCode();
        this.description = errorCode.getDescription();
    }
}
