package com.cl.exception;

import com.cl.common.BaseResponse;
import com.cl.common.ErrorCode;
import com.cl.common.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.InputStream;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler{

    @ExceptionHandler(BaseException.class)
    public BaseResponse baseExceptionHandler(BaseException e){
        log.error("BaseException" + e.getMessage(),e);
        return ResultUtils.error(e.getCode(),e.getMessage(),e.getDescription());
    }

    @ExceptionHandler(RuntimeException.class)
    public BaseResponse runtimeExceptionHandler(RuntimeException e){
        log.error("runtimeException" ,e);
        return ResultUtils.error(ErrorCode.SYSTEM_ERROR,"系统内部异常");

    }

}
