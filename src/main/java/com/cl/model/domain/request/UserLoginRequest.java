package com.cl.model.domain.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录请求
 *
 * @author caolu
 * @date 2022/09/04
 */
@Data
public class UserLoginRequest implements Serializable {

    private static final long serialVersionUID = 3272886781408804155L;

    private String userAccount;

    private String userPassword;
}
