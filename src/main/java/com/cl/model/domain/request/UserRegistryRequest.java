package com.cl.model.domain.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求
 *
 * @author caolu
 * @date 2022/09/04
 */
@Data
public class UserRegistryRequest implements Serializable {

    private static final long serialVersionUID = 3581051274604048289L;

    private String userAccount;

    private String userPassword;

    private String checkPassword;
}
